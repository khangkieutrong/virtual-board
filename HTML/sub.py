import time
import RPi.GPIO as GPIO
import paho.mqtt.client as paho
from threading import Thread
import threading
import random


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)
GPIO.setup(8, GPIO.OUT)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(12, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)
GPIO.setup(20, GPIO.OUT)
GPIO.setup(21, GPIO.OUT)
GPIO.setup(3, GPIO.OUT)
GPIO.setup(2, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)
GPIO.setup(26, GPIO.OUT)
GPIO.setup(14, GPIO.OUT)
GPIO.setup(10, GPIO.OUT)
GPIO.setup(11, GPIO.OUT)
GPIO.setup(9, GPIO.OUT)

GPIO.setup(4, GPIO.IN)                    # initialize GPIO Pins as input
GPIO.setup(17, GPIO.IN)
GPIO.setup(27, GPIO.IN)
GPIO.setup(22, GPIO.IN)
GPIO.setup(5, GPIO.IN)
GPIO.setup(6, GPIO.IN)
GPIO.setup(13, GPIO.IN)
GPIO.setup(19, GPIO.IN)


class myThread(Thread):
        """docstring for myThread"""
        def Thread_Send_ADC():
                broker = 'broker.emqx.io'
                port = 8083
                topic = "voltage"
                client_id = f'python-mqtt-{random.randint(0, 1000)}'

                def connect_mqtt():
                        def on_connect(client, userdata, flags, rc):
                                if rc == 0:
                                        print("Connected to MQTT Broker!")

                                else:
                                        print(
                                            "Failed to connect, return code %d\n", rc)

                        client = paho.Client(client_id, transport="websockets")
                        # client.username_pw_set(username, password)
                        client.on_connect = on_connect
                        client.connect(broker, port)
                        return client

                def ADC(client):
                        x = 1
                        b0 = 0                                    # integers for storing 8 bits
                        b1 = 0
                        b2 = 0
                        b3 = 0
                        b4 = 0
                        b5 = 0
                        b6 = 0
                        b7 = 0
                        vol = 0
                        while True:
                                if (GPIO.input(19) == True):
                                        time.sleep(0.001)
                                        if (GPIO.input(19) == True):
                                                b7 = 1                                       # if pin19 is high bit7 is true

                                if (GPIO.input(13) == True):
                                        time.sleep(0.001)
                                        if (GPIO.input(13) == True):
                                                b6 = 1                                      # if pin13 is high bit6 is true

                                if (GPIO.input(6) == True):
                                        time.sleep(0.001)
                                        if (GPIO.input(6) == True):
                                                b5 = 1                                      # if pin6 is high bit5 is true

                                if (GPIO.input(5) == True):
                                        time.sleep(0.001)
                                        if (GPIO.input(5) == True):
                                                b4 = 1                                     # if pin5 is high bit4 is true

                                if (GPIO.input(22) == True):
                                        time.sleep(0.001)
                                        if (GPIO.input(22) == True):
                                                b3 = 1                                     # if pin22 is high bit3 is true

                                if (GPIO.input(27) == True):
                                        time.sleep(0.001)
                                        if (GPIO.input(27) == True):
                                                b2 = 1                                    # if pin27 is high bit2 is true

                                if (GPIO.input(17) == True):
                                        time.sleep(0.001)
                                        if (GPIO.input(17) == True):
                                                b1 = 1                                    # if pin17 is high bit1 is true

                                if (GPIO.input(4) == True):
                                        time.sleep(0.001)
                                        if (GPIO.input(4) == True):
                                                b0 = 1                                    # if pin4 is high bit0 is true

                                x = (1*b0)+(2*b1)
                                x = x+(4*b2)+(8*b3)
                                x = x+(16*b4)+(32*b5)
                                # representing the bit values from LSB to MSB
                                x = x+(64*b6)+(128*b7)
                                y = (x/255)*5
                                vol = round(y, 2)

                                  #      send_ADC['voltage']=vol
                                b0 = b1 = b2 = b3 = b4 = b5 = b6 = b7 = 0        # reset values

                                msg_count = vol
                                msg = f"{vol} V"
                                result = client.publish(topic, msg)
                                # result: [0, 1]
                                status = result[0]
                                if status == 0:
                                        print(
                                            f"Send `{msg}` to topic `{topic}`")
                                else:
                                        print(
                                        f"Failed to send message to topic {topic}")
                                time.sleep(0.8)

                def run():
                        client = connect_mqtt()
                        client.loop_start()
                        ADC(client)
                run()

        def Thread_Receive_Web():
                broker = "192.168.1.51"
                topic = "gpio"

                def on_connect(client, userdata, flags, rc):
                        print("Connected with result code {0}".format(str(rc)))
                        client.subscribe(topic)

                def on_message(client, userdata, msg):
                        print(topic + str(msg.payload.decode("utf-8")))

                        if msg.topic + str(msg.payload.decode("utf-8")) == "gpioA0_on":
                                GPIO.output(9, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA0_off":
                                GPIO.output(9, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA1_on":
                                GPIO.output(10, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA1_off":
                                GPIO.output(10, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA2_on":
                                GPIO.output(3, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA2_off":
                                GPIO.output(3, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA3_on":
                                GPIO.output(2, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA3_off":
                                GPIO.output(2, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA4_on":
                                GPIO.output(14, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA4_off":
                                GPIO.output(14, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA5_on":
                                GPIO.output(15, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA5_off":
                                GPIO.output(15, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA6_on":
                                GPIO.output(18, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA6_off":
                                GPIO.output(18, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA7_on":
                                GPIO.output(23, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA7_off":
                                GPIO.output(23, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA8_on":
                                GPIO.output(24, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA8_off":
                                GPIO.output(24, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA9_on":
                                GPIO.output(8, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA9_off":
                                GPIO.output(8, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA10_on":
                                GPIO.output(7, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA10_off":
                                GPIO.output(7, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA11_on":
                                GPIO.output(12, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA11_off":
                                GPIO.output(12, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA12_on":
                                GPIO.output(16, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA12_off":
                                GPIO.output(16, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA13_on":
                                GPIO.output(20, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA13_off":
                                GPIO.output(20, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA14_on":
                                GPIO.output(21, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA14_off":
                                GPIO.output(21, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA15_on":
                                GPIO.output(26, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA15_off":
                                GPIO.output(26, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA1_2_on":
                                GPIO.output(14, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA1_2_off":
                                GPIO.output(14, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA2_2_on":
                                GPIO.output(15, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA2_2_off":
                                GPIO.output(15, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA5_2_on":
                                GPIO.output(18, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA5_2_off":
                                GPIO.output(18, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA6_2_on":
                                GPIO.output(23, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA6_2_off":
                                GPIO.output(23, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA3_2_on":
                                GPIO.output(8, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA3_2_off":
                                GPIO.output(8, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA4_2_on":
                                GPIO.output(7, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA4_2_off":
                                GPIO.output(7, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA7_2_on":
                                GPIO.output(12, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA7_2_off":
                                GPIO.output(12, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA8_2_on":
                                GPIO.output(16, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioA8_2_off":
                                GPIO.output(16, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB0_on":
                                GPIO.output(2, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB0_off":
                                GPIO.output(2, GPIO.LOW)
                                print("ledoff")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB1_on":
                                GPIO.output(14, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB1_off":
                                GPIO.output(14, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB2_on":
                                GPIO.output(15, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB2_off":
                                GPIO.output(15, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB3_on":
                                GPIO.output(18, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB3_off":
                                GPIO.output(18, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB4_on":
                                GPIO.output(21, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB4_off":
                                GPIO.output(21, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB5_on":
                                GPIO.output(26, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB5_off":
                                GPIO.output(26, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB6_on":
                                GPIO.output(11, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB6_off":
                                GPIO.output(11, GPIO.LOW)
                                print("ledoff")

                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB7_on":
                                GPIO.output(9, GPIO.HIGH)
                                print("ledon")
                        elif msg.topic + str(msg.payload.decode("utf-8")) == "gpioB7_off":
                                GPIO.output(9, GPIO.LOW)
                                print("ledoff")

                client = paho.Client("digi_mqtt_test")
                client.on_connect = on_connect
                client.on_message = on_message

                client.username_pw_set(username="username", password="pi")
                client.connect(broker)
                client.loop_forever()

try:
        t = time.time()
        t1 = threading.Thread(target=Thread_Send_ADC)
        t2 = threading.Thread(target=Thread_Receive_Web)
        t1.start()
        t2.start()
except:
        print ("Error: unable to start thread")
